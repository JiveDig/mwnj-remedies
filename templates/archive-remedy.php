<?php

// Enqueue listjs scripts
add_action( 'wp_enqueue_scripts', 'mwnj_do_listjs_scripts' );
function mwnj_do_listjs_scripts() {
	wp_enqueue_script( 'mixitup', MWNJ_REMEDIES_URL . 'assets/jquery.mixitup.min.js', array( 'jquery' ), '', true );
	wp_enqueue_script( 'mixitup-init', MWNJ_REMEDIES_URL . 'assets/mixitup-init.js', array( 'mixitup' ), '', true );
}

//* Remove the entry meta in the entry header (requires HTML5 theme support)
remove_action( 'genesis_entry_header', 'genesis_post_info', 5 ); // This was set to 5 in functions.php

// Remove the entry footer markup
remove_action( 'genesis_entry_footer', 'genesis_entry_footer_markup_open', 5 );
remove_action( 'genesis_entry_footer', 'genesis_entry_footer_markup_close', 15 );

// Remove the entry meta in the entry footer
remove_action( 'genesis_entry_footer', 'genesis_post_meta' );

// Add MixItUp container id to .content
add_filter( 'genesis_attr_content', 'mwnjr_mixitup_content_container' );
function mwnjr_mixitup_content_container( $attributes ) {
    $attributes['id'] = 'remedies-content';
    return $attributes;
}

// Add MixItUp data attributes to .entry
add_filter( 'genesis_attr_entry', 'jive_attributes_st_content_inner' );
function jive_attributes_st_content_inner( $attributes ) {

    $tags = get_the_terms( $post->ID, 'remedy_tag' );
    if ( $tags ) {
        foreach( $tags as $tag ) {
            $slug_tag = ' mix-' . $tag->slug;
        }
    } else {
        $slug_tag = '';
    }

    $attributes['class'] = $attributes['class'] . ' mix' . $slug_tag;

    return $attributes;
}

/**
 * Add filter links
 * @uses MixItUp.js
 * @link https://mixitup.kunkalabs.com/
 */
add_action( 'genesis_before_loop', 'mwnjr_remedy_filters' );
function mwnjr_remedy_filters() {

    $args_tags = array(
        'orderby'           => 'name',
        'order'             => 'ASC',
        'hide_empty'        => true,
    );

    $tags  = get_terms( 'remedy_tag', $args_tags );

    if ( ! empty($tags) ) {
    ?>
        <div id="remedies-filter">
            Sort by:
            <?php foreach ($tags as $tag ) { ?>
                <span class="filter" data-filter=".<?php echo 'mix-' . $tag->slug; ?>"><?php echo $tag->name; ?></span>
            <?php } ?>
        </div>
    <?php
    }
}

// Voodoo
genesis();
