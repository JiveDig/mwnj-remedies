<?php
/**
 * Plugin Name: Midwives of NJ - Remedies
 * Plugin URI: http://midwivesofnj.com
 * Description: Remedies section of Midwives of NJ
 * Version: 1.0.0
 * Author: Mike Hemberger
 * Author URI: http://thestizmedia.com
 * Requires at least: 4.0
 * Tested up to: 4.0
 *
 * Text Domain: midwivesofnj
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

// Plugin and class main setup
if ( ! class_exists( 'MWNJ_Remedies' ) ) {

	final class MWNJ_Remedies {

		/**
		 * Holds the instance
		 *
		 * Ensures that only one instance of MWNJ_Remedies exists in memory at any one
		 * time and it also prevents needing to define globals all over the place.
		 *
		 * TL;DR This is a static property property that holds the singleton instance.
		 *
		 * @var object
		 * @static
		 * @since 1.0
		 */
		private static $instance;

		/**
		 * Main Instance
		 *
		 * Ensures that only one instance exists in memory at any one
		 * time. Also prevents needing to define globals all over the place.
		 *
		 * @since 1.0
		 *
		 */
		public static function get_instance() {
			if ( ! isset( self::$instance ) && ! ( self::$instance instanceof MWNJ_Remedies ) ) {
				self::$instance = new MWNJ_Remedies;
				self::$instance->setup_globals();
				self::$instance->includes();

			}

			return self::$instance;
		}

		/**
		 * Constructor Function
		 *
		 * @since 1.0
		 * @access private
		 * @see MWNJ_Remedies::init()
		 * @see MWNJ_Remedies::activation()
		 */
		private function __construct() {
			self::$instance = $this;

			add_action( 'init', array( $this, 'init' ) );
		}

		/**
		 * Reset the instance of the class
		 *
		 * @since 1.0
		 * @access public
		 * @static
		 */
		public static function reset() {
			self::$instance = null;
		}

		/**
		 * Globals
		 *
		 * @since 1.0
		 * @return void
		 */
		private function setup_globals() {

			// Plugin Folder Path
			if ( ! defined( 'MWNJ_REMEDIES_VERSION' ) ) {
				define( 'MWNJ_REMEDIES_VERSION', '1.0.0' );
			}
			// Plugin Folder Path
			if ( ! defined( 'MWNJ_REMEDIES_TITLE' ) ) {
				define( 'MWNJ_REMEDIES_TITLE', 'Midwives of NJ - Remedies' );
			}
			// Plugin Folder Path
			if ( ! defined( 'MWNJ_REMEDIES_DIR' ) ) {
				define( 'MWNJ_REMEDIES_DIR', plugin_dir_path( __FILE__ ) );
			}
			// Plugin Folder URL
			if ( ! defined( 'MWNJ_REMEDIES_URL' ) ) {
				define( 'MWNJ_REMEDIES_URL', plugin_dir_url( __FILE__ ) );
			}
			// Plugin Root File
			if ( ! defined( 'MWNJ_REMEDIES_FILE' ) ) {
				define( 'MWNJ_REMEDIES_FILE', __FILE__ );
			}

		}

		/**
		 * Function fired on init
		 *
		 * This function is called on WordPress 'init'. It's triggered from the
		 * constructor function.
		 *
		 * @since 1.0
		 * @access public
		 *
		 * @uses MWNJ_Remedies::load_textdomain()
		 *
		 * @return void
		 */
		public function init() {
			// If we need this later
		}

		/**
		 * Includes
		 *
		 * @since 1.0
		 * @access private
		 * @return void
		 */
		private function includes() {
			require_once( MWNJ_REMEDIES_DIR . 'includes/post-types.php' );
			require_once( MWNJ_REMEDIES_DIR . 'includes/functions.php' );
		}

	}

} // MWNJ_Remedies

function MWNJ_Remedies() {
	return MWNJ_Remedies::get_instance();
}

MWNJ_Remedies();
