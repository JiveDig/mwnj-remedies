jQuery(function( $ ){
	$('#remedies-content').mixItUp({
		animation: {
			// enable: false,
			duration: 200,
			effects: 'fade',
		},
		layout: {
			display: 'block'
		}
	});
});