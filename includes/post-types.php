<?php

// Register 'remedy' CPT
add_action( 'init', 'mwnj_register_remedy_cpt', 0 );
function mwnj_register_remedy_cpt() {

	$labels = array(
		'name'                => _x( 'Remedies', 'Post Type General Name', 'midwivesofnj' ),
		'singular_name'       => _x( 'Remedy', 'Post Type Singular Name', 'midwivesofnj' ),
		'menu_name'           => __( 'Remedies', 'midwivesofnj' ),
		'name_admin_bar'      => __( 'Remedies', 'midwivesofnj' ),
		'parent_item_colon'   => __( 'Parent Item:', 'midwivesofnj' ),
		'all_items'           => __( 'All Items', 'midwivesofnj' ),
		'add_new_item'        => __( 'Add New Item', 'midwivesofnj' ),
		'add_new'             => __( 'Add New', 'midwivesofnj' ),
		'new_item'            => __( 'New Item', 'midwivesofnj' ),
		'edit_item'           => __( 'Edit Item', 'midwivesofnj' ),
		'update_item'         => __( 'Update Item', 'midwivesofnj' ),
		'view_item'           => __( 'View Item', 'midwivesofnj' ),
		'search_items'        => __( 'Search Item', 'midwivesofnj' ),
		'not_found'           => __( 'Not found', 'midwivesofnj' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'midwivesofnj' ),
	);

	$rewrite = array(
		'slug'                => 'remedies',
		'with_front'          => true,
		'pages'               => true,
		'feeds'               => true,
	);

	$args = array(
		'label'               => __( 'remedy', 'midwivesofnj' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'thumbnail', 'genesis-cpt-archives-settings' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 5,
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'rewrite'             => $rewrite,
		'capability_type'     => 'page',
	);
	register_post_type( 'remedy', $args );

}


// Register Custom Taxonomy
add_action( 'init', 'mwnj_remedy_categories_taxonomy', 0 );
function mwnj_remedy_categories_taxonomy() {

	$labels = array(
		'name'                       => _x( 'Remedy Categories', 'Taxonomy General Name', 'midwivesofnj' ),
		'singular_name'              => _x( 'Remedy Category', 'Taxonomy Singular Name', 'midwivesofnj' ),
		'menu_name'                  => __( 'Remedy Categories', 'midwivesofnj' ),
		'all_items'                  => __( 'All Items', 'midwivesofnj' ),
		'parent_item'                => __( 'Parent Item', 'midwivesofnj' ),
		'parent_item_colon'          => __( 'Parent Item:', 'midwivesofnj' ),
		'new_item_name'              => __( 'New Item Name', 'midwivesofnj' ),
		'add_new_item'               => __( 'Add New Item', 'midwivesofnj' ),
		'edit_item'                  => __( 'Edit Item', 'midwivesofnj' ),
		'update_item'                => __( 'Update Item', 'midwivesofnj' ),
		'view_item'                  => __( 'View Item', 'midwivesofnj' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'midwivesofnj' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'midwivesofnj' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'midwivesofnj' ),
		'popular_items'              => __( 'Popular Items', 'midwivesofnj' ),
		'search_items'               => __( 'Search Items', 'midwivesofnj' ),
		'not_found'                  => __( 'Not Found', 'midwivesofnj' ),
	);
	$rewrite = array(
		'slug'                       => 'remedy-category',
		'with_front'                 => true,
		'hierarchical'               => false,
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
		'rewrite'                    => $rewrite,
	);
	register_taxonomy( 'remedy_cat', array( 'remedy' ), $args );

}

// Register Custom Taxonomy
add_action( 'init', 'mwnj_remedy_tags_taxonomy', 0 );
function mwnj_remedy_tags_taxonomy() {

	$labels = array(
		'name'                       => _x( 'Remedy Tags', 'Taxonomy General Name', 'midwivesofnj' ),
		'singular_name'              => _x( 'Remedy Tag', 'Taxonomy Singular Name', 'midwivesofnj' ),
		'menu_name'                  => __( 'Remedy Tags', 'midwivesofnj' ),
		'all_items'                  => __( 'All Items', 'midwivesofnj' ),
		'parent_item'                => __( 'Parent Item', 'midwivesofnj' ),
		'parent_item_colon'          => __( 'Parent Item:', 'midwivesofnj' ),
		'new_item_name'              => __( 'New Item Name', 'midwivesofnj' ),
		'add_new_item'               => __( 'Add New Item', 'midwivesofnj' ),
		'edit_item'                  => __( 'Edit Item', 'midwivesofnj' ),
		'update_item'                => __( 'Update Item', 'midwivesofnj' ),
		'view_item'                  => __( 'View Item', 'midwivesofnj' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'midwivesofnj' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'midwivesofnj' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'midwivesofnj' ),
		'popular_items'              => __( 'Popular Items', 'midwivesofnj' ),
		'search_items'               => __( 'Search Items', 'midwivesofnj' ),
		'not_found'                  => __( 'Not Found', 'midwivesofnj' ),
	);
	$rewrite = array(
		'slug'                       => 'remedy-tags',
		'with_front'                 => true,
		'hierarchical'               => false,
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => false,
		'show_admin_column'          => false,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
		'rewrite'                    => $rewrite,
	);
	register_taxonomy( 'remedy_tag', array( 'remedy' ), $args );

}
