<?php

// Use our cpt archive template
add_filter('template_include', 'mwnj_do_remedies_template');
function mwnj_do_remedies_template( $template ) {

	// Bail if not our post type archive
	if ( ! is_post_type_archive('remedy') ) {
		return $template;
	}

	return MWNJ_REMEDIES_DIR . 'templates/archive-remedy.php';

}

// Auto add post tag of uppercase first letter of title
add_action( 'save_post_remedy', 'mwnjr_do_custom_post_meta' );
function mwnjr_do_custom_post_meta() {
	$title = get_the_title();
	wp_set_object_terms( get_the_ID(), strtoupper( $title[0] ), 'remedy_tag' );
}
